export const REGISTRATION_TOKEN_PLACEHOLDER = '$REGISTRATION_TOKEN';

export const PLATFORM_DOCKER = 'docker';
export const PLATFORM_KUBERNETES = 'kubernetes';
